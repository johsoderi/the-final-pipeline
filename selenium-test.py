from selenium import webdriver

driver = webdriver.Chrome('/usr/bin/chromedriver')
driver.get("http://localhost:8080")

oldText = driver.find_element_by_id("txt")
assert "even number" in oldText.text

clickyThing = driver.find_element_by_id("clickything")
clickyThing.click()

newText = driver.find_element_by_id("txt")
assert "Goodbye" in newText.text

driver.close()

