#!/usr/bin/python3
from http.server import BaseHTTPRequestHandler, HTTPServer
import random
import sys


class EvenRand():
    def getRand():
        return random.randrange(0, 1337, 2)


class Server(BaseHTTPRequestHandler, EvenRand):

    def do_GET(self):
        self.numba = EvenRand.getRand()
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.text = "<p id=txt>Here's an even number for you: " + \
            str(self.numba) + \
            "</p><button id=clickything onclick=\"changeText()\"" + \
            " type=\"button\">Don't click me!</button>" + \
            "<script>function changeText() " + \
            "{document.getElementById(\"txt" + \
            "\").innerHTML = \"Goodbye!\";}</script>"
        self.wfile.write(self.text.encode('UTF-8'))
        return


def runServer():
    try:
        server = HTTPServer(('', 8080), Server)
        print('Started httpserver on port 8080')
        server.serve_forever()

    except KeyboardInterrupt:
        print('Keyboard interrupt, shutting down!')
        server.socket.close()


try:
    if sys.argv[1] == "run":
        runServer()

except IndexError:
    print("To start server, run 'python3 ./serve.py run'")
